//
//  NewsDetailsVC.swift
//  NewsFeedApp
//
//  Created by Loci Olah on 18.10.2021.
//

import UIKit

class NewsDetailsVC: UIViewController {
    
    @IBOutlet weak var Title1: UILabel!
    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var image: UIImageView!
    
    var newsDetailsSelected: News = .init(image: UIImage(named: ""), title: "", smallInfo: "", fullInfo: .fullimfo1)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.image = newsDetailsSelected.image
        Title1.text = newsDetailsSelected.title
        TextView.text = newsDetailsSelected.fullInfo.rawValue
    }
    
    @IBAction func back (_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
