//
//  SignUpViewController.swift
//  NewsFeedApp
//
//  Created by Яков on 12.10.2021.
//

import UIKit

enum ErrorMessage: String {
    case textFieldIsEmpty = "Please, fill in all the fields!"
    case nameTaken = "A user with this name already exists,\n try another name"
}

class SignUpViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        errorLabel.isHidden = true
    }
    
    deinit { debugPrint("🏁 - \(classForCoder)") }
    
    @IBAction func addNewUserAction(_ sender: Any) {
        addingNewUser(userNameTextField, passwordTextField)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - function - user verification and adding a New User

extension SignUpViewController {
    
    func addingNewUser(_ name: UITextField, _ pass: UITextField) {

        if checkingTextFields(name, pass) && !nameVerification(name) {
            UserManager.shared.generateNewUser(userNameTextField.text ?? "newUser", passwordTextField.text ?? "newUser")
            goToTheNewsFeedScreen()
        } else {
            errorMessage(nameVerification(name), checkingTextFields(name, pass))
        }
    }
    
    func nameVerification(_ name: UITextField) -> (Bool) {
        var checkUserName: Bool = false
        
        for item in UserManager.shared.users {
            if name.text == item.userName {
                checkUserName = true
            }
        }
        return checkUserName
    }
    
    func checkingTextFields(_ name: UITextField, _ pass: UITextField) -> (Bool) {
        var check: Bool = false

        if !(name.text == "") && !(pass.text == "") && !(name.text == nil) && !(pass.text == nil) {
            check = true
        }
        return check
    }

    func goToTheNewsFeedScreen() {
        let vcSignIn = NewsFeedViewController(nibName: "NewsFeedViewController", bundle: nil)
        navigationController?.pushViewController(vcSignIn, animated: true)
    }
    
    func errorMessage(_ errorNameTaken: Bool, _ errortextFieldIsEmpty: Bool) {
        errorLabel.isHidden = false
        
        if !errortextFieldIsEmpty {
            errorLabel.text = ErrorMessage.textFieldIsEmpty.rawValue
        } else if errorNameTaken {
            errorLabel.text = ErrorMessage.nameTaken.rawValue
            userNameTextField.text = ""
            passwordTextField.text = ""
        }
    }
}

//MARK: - function - activating the button next to the textField

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case userNameTextField:
            errorLabel.isHidden = true
            if !nameVerification(userNameTextField) {
                passwordTextField.becomeFirstResponder()
            } else {
                errorMessage(nameVerification(userNameTextField), true)
            }
            
        default:
            passwordTextField.resignFirstResponder()
            addingNewUser(userNameTextField, passwordTextField)
        }
        return true
    }
}
