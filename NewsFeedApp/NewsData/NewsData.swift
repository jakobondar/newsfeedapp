//
//  NewsData.swift
//  NewsFeedApp
//
//  Created by Nazar Lysak on 14.10.2021.
//

import Foundation
import UIKit

enum FullInfo: String {
    
    case fullimfo1 = "Stephen Sackur speaks to president of Ukraine Volodymyr Zelensky, who played the role on TV before getting the job in real life, to see how he is doing on his promise to end corruption."
    case fullimfo2 = "2"
    case fullimfo3 = "3"
    case fullimfo4 = "4"
    case fullimfo5 = "5"
    case fullimfo6 = "6"
    case fullimfo7 = "7"    
}

struct News {
    var image: UIImage!
    var title: String
    var smallInfo: String
    var fullInfo: FullInfo
}

class NewsManager {
    func loadNews() -> [News] {
        
        let news1 = News(image: UIImage(named: "1"), title: "Volodymyr Zelensky - President of Ukraine", smallInfo: "Stephen Sackur speaks to president of Ukraine Volodymyr Zelensky, who played the role on TV before getting the job in real life, to see how he is doing on his promise to end corruption.", fullInfo: .fullimfo1)
        
        let news2 = News(image: UIImage(named: "2"), title: "Microsoft shutting down LinkedIn in China", smallInfo: "Microsoft is shutting down its social network, LinkedIn, in China, saying having to comply with the Chinese state has become increasingly challenging.", fullInfo: .fullimfo2)
        
        let news3 = News(image: UIImage(named: "3"), title: "North Korea's Kim Jong-un faces 'paradise on Earth' lawsuit", smallInfo: "North Korea's leader Kim Jong-un should pay damages for a 1959-84 scheme that saw more than 90,000 people move there from Japan, a Tokyo court is hearing.", fullInfo: .fullimfo3)
        
        let news4 = News(image: UIImage(named: "4"), title: "Afghanistan: Pakistan airline stops flights citing Taliban intimidation", smallInfo: "Pakistan International Airlines has suspended flights to the Afghan capital Kabul, citing 'heavy-handed' interference from the Taliban.", fullInfo: .fullimfo4)
        
        let news5 = News(image: UIImage(named: "5"), title: "Coldplay: Band ready for backlash over eco-friendly world tour", smallInfo: "Coldplay's next tour will partly be powered by a dancefloor that generates electricity when fans jump up and down, and pedal power at the venues.", fullInfo: .fullimfo5)
        
        let news6 = News(image: UIImage(named: "6"), title: "Kongsberg: Bow and arrow attack appears to be terrorism - officials", smallInfo: "A deadly bow and arrow attack in Norway which left five people dead appears to have been an act of terror, Norway's security service (PST) said.", fullInfo: .fullimfo6)
        
        let news7 = News(image: UIImage(named: "7"), title: "Regeni murder: Egyptians go on trial for Italian student's murder", smallInfo: "Four members of Egypt's security forces have gone on trial in absentia in Rome, accused of kidnapping, torturing and killing an Italian student.", fullInfo: .fullimfo7)
        
        return [news1, news2, news3, news4, news5, news6, news7]
    }
}
