//
//  ProfileData.swift
//  NewsFeedApp
//
//  Created by Яков on 12.10.2021.
//

import Foundation
import FileProvider

class UserManager {
    
    static let shared = UserManager()

    class User {
        
        init(userName: String, password: String) {
            self.userName = userName
            self.password = password
        }
        var userName: String
        var password: String
    }
    
    var users: [User] = [User(userName: "test", password: "test")]
    
    func generateNewUser(_ userName: String, _ password: String) {
        users.append(User(userName: userName, password: password))
    }
}
