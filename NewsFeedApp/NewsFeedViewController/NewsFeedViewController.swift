//
//  NewsFeedViewController.swift
//  NewsFeedApp
//
//  Created by Яков on 12.10.2021.
//

import UIKit

class NewsFeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var allNews: [News] = NewsManager().loadNews()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .none
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.reloadData()
    }
}

extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allNews.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        cell.update(news: allNews[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vcNewsDetails = NewsDetailsVC(nibName: "NewsDetailsVC", bundle: nil)
        vcNewsDetails.newsDetailsSelected = allNews[indexPath.row]
        
        navigationController?.pushViewController(vcNewsDetails, animated: true)
    }
}
