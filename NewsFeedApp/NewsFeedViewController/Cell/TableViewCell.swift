//
//  TableViewCell.swift
//  NewsFeedApp
//
//  Created by Nazar Lysak on 14.10.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newTitleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(news: News) {
        newsImage.image = news.image
        newTitleLabel.text = news.title
        infoLabel.text = news.smallInfo
    }    
}
