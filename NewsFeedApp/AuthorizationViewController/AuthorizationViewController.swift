//
//  AuthorizationViewController.swift
//  NewsFeedApp
//
//  Created by Яков on 12.10.2021.
//

import UIKit

class AuthorizationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        navigationController?.pushViewController(SignUpViewController(nibName: "SignUpViewController", bundle: nil), animated: true)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        navigationController?.pushViewController(SignInViewController(nibName: "SignInViewController", bundle: nil), animated: true)
    }
}
