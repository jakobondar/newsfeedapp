//
//  SignInViewController.swift
//  NewsFeedApp
//
//  Created by Яков on 12.10.2021.
//

import UIKit

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var enterNameTextField: UITextField!
    @IBOutlet weak var pageLogoImage: UIImageView!
    @IBOutlet weak var enterPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageLogoImage.image = UIImage(named: "signInIcon")
        enterNameTextField.delegate = self
        enterPasswordTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == enterNameTextField{
            enterPasswordTextField.becomeFirstResponder()
        } else {
            enterPasswordTextField.resignFirstResponder()
        }
        return true
    }
    
    func checkInfo() -> Bool {
        for user in UserManager.shared.users {
            if user.userName == enterNameTextField.text && user.password == enterPasswordTextField.text {
                return true
            }
        }
        return false
    }

    
    @IBAction func signInAction(_ sender: Any) {
        if checkInfo() {
            navigationController?.pushViewController(NewsFeedViewController(nibName: "NewsFeedViewController", bundle: nil), animated: true)
        } else {
            let alert = UIAlertController(title: "Incorrect password", message: "Do you want to sign up", preferredStyle: .alert)
            let signUpAction = UIAlertAction(title: "Sign up", style: .default){ _
                in
                let nextVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
                self.navigationController?.pushViewController(nextVC, animated: false)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(signUpAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
